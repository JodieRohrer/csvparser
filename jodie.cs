using System;
using System.IO;
using System.Collections.Generic;

namespace CSVReader
{
    public static class Jodie
    {
        public static void RequestMetricsTable()
        {
            

            string[] lines = null; 

            List<string> outgoing = new List<string>();
            //"new Thing(){propert1=sdf, propert1=sdf, propert1=sdf}"
            string code = "";
            try{
                lines = File.ReadAllLines(@"E:\College\CIDM4390\parser\input.csv");
                foreach(string line in lines){
                    code = "new RequestMetrics(){";                
                    //Console.WriteLine(line);
                    string[] parts = line.Split(',');
                    code += $"RequestMetricId={parts[0]}, ";
                    code += $"Timestamp={parts[1]}, ";
                    code += $"SourceIp=\"{parts[2]}\", ";
                    code += $"SourcePort={parts[3]}, ";
                    code += $"URI=\"{parts[4]}\", ";
                    code += $"Verb=\"{parts[5]}\", ";
                    code += $"RequestBody=\"{parts[6]}\", ";
                    code += $"ReasonPhrase=\"{parts[7]}\", ";
                    code += $"ResponseBody=\"{parts[8]}\", ";
                    code += $"ResponseCode={parts[9]}, ";
                    code += $"Duration={parts[10]}, ";
                    code += $"Channel={parts[11]}, ";
                    code += $"APIVersion=\"{parts[12]}\", ";
                    code += $"APIController=\"{parts[13]}\", ";
                    code += $"APIMethod=\"{parts[14]}\", ";
                    code += $"BankNumber={parts[15]}, ";
                    code += $"ServerName=\"{parts[16]}\", ";
                    code += $"UserID={parts[17]}, ";
                    code += $"EmpID={parts[18]}, ";
                    code += $"SessionID={parts[19]}, ";
                    code += $"RequestCert=\"{parts[20]}\", ";
                    code += $"PartnerKey=\"{parts[21]}\"";
                    code +="},";
                    code += Environment.NewLine;
                    outgoing.Add(code);
                }

                // int index = -999;
                // Console.WriteLine("BEFORE");
                // Console.WriteLine(outgoing[outgoing.Count - 1]);
                // index = outgoing[outgoing.Count - 1].LastIndexOf(',');
                // Console.WriteLine($"{index}");
                
                // outgoing[outgoing.Count - 1] = outgoing[outgoing.Count - 1].Remove(outgoing[outgoing.Count - 1].LastIndexOf(','));
                // Console.WriteLine("AFTER");
                // Console.WriteLine(outgoing[outgoing.Count - 1]);                
                
                File.WriteAllLines(@"E:\College\CIDM4390\parser\code.txt", outgoing.ToArray());

            }catch(Exception exp){
                Console.Error.WriteLine(exp.Message);
            }
        }
    }
}


